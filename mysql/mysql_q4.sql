CREATE TABLE rectangles(
                           id INTEGER NOT NULL PRIMARY KEY,
                           width INTEGER NOT NULL,
                           height INTEGER NOT NULL);


INSERT rectangles
VALUES (1, 10, 20),
       (2, 10, 20),
       (3, 4, 20),
       (4, 4, 20),
       (5, 100, 20);


SELECT width * height AS AREA, COUNT(*)
FROM rectangles
GROUP BY width * height;