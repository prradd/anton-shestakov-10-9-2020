CREATE TABLE companies(
                          id INTEGER PRIMARY KEY,
                          name VARCHAR (30) NOT NULL,
                          country VARCHAR (30) NOT NULL);

INSERT companies
VALUES (1,'Toyota','Japan'),
       (2,'Hunday','China'),
       (3,'Shevrolet','USA'),
       (4,'Jaguar','USA');

SELECT DISTINCT country
FROM companies
ORDER by 1 ASC;