CREATE TABLE menu(
                     itemName VARCHAR (50) PRIMARY KEY NOT NULL,
                     category VARCHAR (50) NOT NULL,
                     price DECIMAL (5,2) );

INSERT menu
VALUES ('Cesar', 'Salads', 20),
       ('Tomato', 'Salads', 25),
       ('Mashrooms', 'Soups', 50),
       ('Chiken', 'Lunch', 110);

UPDATE menu
SET price = price * 1.10
WHERE category IN ( 'Soups','Salads');