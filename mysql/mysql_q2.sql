CREATE TABLE  fsia (
                       companyName VARCHAR (30) NOT NULL PRIMARY KEY,
                       marketCapitalization FLOAT NOT NULL);

CREATE TABLE fsib(
                     companyName VARCHAR (30) NOT NULL PRIMARY KEY,
                     sharePrice FLOAT NOT NULL,
                     shareOutstanding  INTEGER NOT NULL);
INSERT fsia
VALUES ('Toyota', 1.1),
       ('Hyundai', 2.1),
       ('Chevrolet', 3.1);

INSERT fsib
VALUES ('Toyota', 1000.20,1),
       ('Hyundai',500.6,5),
       ('Chevrolet',200.3,2);



SELECT fsia.companyName,fsia.marketCapitalization
FROM fsib
         JOIN fsia ON fsia.companyName = fsib.companyName
order by fsib.sharePrice * fsib.shareOutstanding DESC;
