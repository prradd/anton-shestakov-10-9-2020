CREATE TABLE employees (

id INTEGER NOT NULL PRIMARY KEY,
mgrId INTEGER REFERENCES employees (id),
name VARCHAR (30) NOT NULL);

INSERT employees
VALUES (4,3,'Mia'),
(1, 2, 'Olga'),
(2,NULL,'Anton'),
(3, NULL, 'Romi');

SELECT e1.name EmloyeeName, e2.name AS MgrName;