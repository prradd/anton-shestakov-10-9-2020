#unieq_names
<!--For example: calling unique_names([‘Dan’,’Ron’,’Boris], [‘Boris’,’Dan’, Ruth’]) should return:-->

<!--[‘Dan’,’Ron’,’Boris,’Ruth’] in any order.-->

<?php

function unique_names(array $arr1, array $arr2): array {

    foreach($arr2 as $name){
       if (!in_array($name, $arr1)){
           array_push($arr1, $name);
       }
    }

    return $arr1;
}

echo '<pre>';

print_r(unique_names(['Dan','Ron','Boris'], ['Boris','Dan', 'Ruth']));
print_r(unique_names(['Anton','Olga','Mia', 'Romy'], ['Ivan','Dimi', 'Anton']));
print_r(unique_names(['Odin','Thor','Heimdal'], ['Loki','Fenrir', 'Jotunn', 'Loki']));

echo '</pre>';

