<div style="font-size: large; font-family: sans-serif; line-height: 1.5">

    The GET data may be accessed through the <b>$_GET</b> or the <b>$_REQUEST</b> associative array. <br><br>

    For example: <br>
    <b>$_GET['count']</b><br>
</div>