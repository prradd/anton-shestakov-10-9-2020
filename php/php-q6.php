
<div style="font-size: large; font-family: sans-serif; line-height: 1.5">
    <b>setcookie()</b> function in PHP, defines a cookie to be sent with the HTTP request. <br>
    It may then be accessed in the <b>$_COOKIE</b> or the <b>$_REQUEST</b> array. <br><br>

    For example: <br>
    <b>setcookie("token", "Amirasdasd")</b> may be then accessed by <b>$_COOKIE['token'] == 'Amirasdasd'</b> <br>
</div>


