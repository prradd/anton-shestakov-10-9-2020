<div style="font-size: large; font-family: sans-serif; line-height: 1.5">

    .htaccess is a configuration file for the Apache web servers. <br>
    We place it in main folder which is loaded by the Apache. The web server will then detect and read the file. <br>
    These configurations may modify the setup of the Apache server, can be used for redirect pages, filter abusers by IP etc.

</div>