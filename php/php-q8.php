<div style="font-size: large; font-family: sans-serif; line-height: 1.5">

    The cookie (assuming it was set earlier) may be accessed through the <b>$_COOKIE</b> or the <b>$_REQUEST</b> associative array. <br><br>

    For example: <br>
    <b>$_COOKIE['token']</b><br>
</div>