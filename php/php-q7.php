<div style="font-size: large; font-family: sans-serif; line-height: 1.5">

    The <b>require</b> construct will emit a fatal error (E_COMPILE_ERROR) and stop the script, when the file to require is missing. <br><br>

    The <b>include</b> construct will only throw a warning (E_WARNING) and will continue with the script execution, if the file to include cannot be found.

</div>