#Filter
<?php

function  filterNumbersFromArray (array &$arr)  :  void
{
    foreach ($arr as $key => $value){
        if (gettype($value) != "integer"){
            unset($arr[$key]);
        }
    }
}

$arr = [1, "a", "b", 2, null, 5, 5.6];
filterNumbersFromArray ($arr) ;

echo '<pre>';
print_r ( array_values ( $arr ) ) ;
echo '</pre>';