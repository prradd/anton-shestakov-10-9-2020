#Filter

<?php

function  filterNumbersFromArray (array &$arr)  :  void
{
    sort($arr);
    $n = count($arr);
    $newArr = [];

    for ($i = 0; $i < count($arr); $i++){
        while ($i < $n - 1 && $arr[$i] == $arr[$i + 1])
            $i++;
        array_push($newArr, $arr[$i]);
    }

    $arr = $newArr;
}

$arr = [1, 1 , 4, 2, 2, 7, 0, 0];
filterNumbersFromArray ($arr) ;

echo '<pre>';
print_r ( array_values ( $arr ) ) ;
echo '</pre>';
