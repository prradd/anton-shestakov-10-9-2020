<div style="font-size: large; font-family: sans-serif; line-height: 1.5">

    To check whether the given value is a number, we can use <b>gettype()</b> function. <br><br>

    Example: <br>

    if (gettype($val) == 'integer' || gettype($val) == 'float'){ <br>
        echo "it's a number"; <br>
    }

</div>